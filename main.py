from preprocess import *
from char_models import *
from trainer import LotRTrainer


def eval(model, gen_text, n_preds, pred_length=50, load_path="./models/model.pt"):
    # Once trained, load best model found
    model.load_state_dict(torch.load(load_path))
    model.eval()


def train(model, gen_text, output_path="./models/model.pt", n_epochs=int(1e4)):
    # Create trainer
    optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-3)
    trainer = LotRTrainer(optimizer=optimizer, n_epochs=n_epochs, model=model,
                          print_every=100, eval_every=100, model_save_path=output_path,
                          debug=False)
    # Train
    trainer.fit(gen_text)


if __name__ == "__main__":
    # TODO: clt
    # TODO: test TCNs
    # TODO: conv 1d
    # Load LotR text corpus
    text = load_data()

    # Convert strings to int
    text = char_to_ind(text)

    # Create infinite generator from text
    seq_length = 60
    n_seqs = 128
    gen_text = gen_X_Y(text, seq_length=seq_length, n_seqs=n_seqs)

    # Create model
    # model = CharRNN(seq_length, n_chars, embed_size=8, gru_units=128, dropout=0.2, debug=False)
    # model = CharConv(seq_length, n_chars, embed_size=8, debug=False)
    # model = CharHybrid(seq_length, n_chars, embed_size=8, gru_layers=1, gru_units=32, n_channels=32, kernel_size=3,
                    #    debug=False)
    model = CharTCN(n_chars, 8, [32, 8, 8], 3, debug=False)

    train(model, gen_text, output_path="./models/test.pt", n_epochs=10000)

