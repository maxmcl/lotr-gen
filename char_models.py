import torch
import torch.nn as nn
import logging
from sys import path as sys_path
# Comparing with TCNs
sys_path.append("../TCN")
from TCN.tcn import TemporalConvNet


class CharBaseModel(nn.Module):
    """
    CharBaseModel
    -------------
    Basic functionalities shared by all character models.
    :params: seq_length (int): sequence length (# of timesteps)
    :params: n_chars (int): total number of characters used in the model (# of classes)
    :params: embed_size (int): size of the embedding to use for the characters
    :params: logger_name (string): name of the logger
    :params: debug (bool): whether debugging info is printed or not
    """
    def __init__(self, seq_length, n_chars, embed_size=8, logger_name="model", debug=False):
        super(CharBaseModel, self).__init__()
        self.seq_length = seq_length  # n of time steps
        self.n_chars = n_chars  # n of features
        self.embed_size = embed_size  # embed the characters into embed_size vectors

        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG if debug else logging.INFO)
        self.logger.addHandler(logging.StreamHandler())
        
        # Learn an embedding in the characters space instead of one-hot vectors
        self.embed = nn.Embedding(self.n_chars, self.embed_size)
        # Use the cross-entropy loss function
        self.loss = nn.CrossEntropyLoss()


    def predict(self, X, pred_length=100, temperature=0.5):
        """
        predict
        -------
        Unfolding calls to self.forward for pred_length characters and sampling from the output
        probability distribution. Each new character is appended to the inputs while the 1st character
        is dropped so the sequence length stays the same.
        :params: X (Torch tensor): input should be a 2D tensor of (n_seqs, seq_length) indices
        :params: pred_length (int): # of steps to unfold. Predicted character will be appended
                                    to the input X.
        :params: temperature (float): denominator of the exponential that introduces more randomness
                                      in the predictions
        :return: (Torch tensor): original sequence with pred_length new indices (characters) appended
        """
        temp = X.clone()
        for step in range(pred_length):
            # Unroll the new predictions for pred_length steps
            # In the case of RNNs we can actually only feed a single character and refeed the
            # previous hidden state.
            # Technically should softmax Y_hat to get probabilities, don't think it matters though.
            Y_hat = self.forward(temp)
            # Sample from the network as a multinomial distribution
            top = torch.multinomial(Y_hat.div(temperature).exp(), 1)
            # Add predicted character to string and use as next input
            temp = torch.cat((temp[:, 1:], top), dim=1)
            X = torch.cat((X, top), dim=1)
        return X

class CharRNN(CharBaseModel):
    """
    CharRNN
    -------
    RNN-based character model of the following structure:
    Input -> Embedding -> GRU layers -> Dropout -> Linear -> Cross-entropy loss
    :params: seq_length (int): sequence length (# of timesteps)
    :params: n_chars (int): total number of characters used in the model (# of classes)
    :params: embed_size (int): size of the embedding to use for the characters
    :params: gru_units (int):
    :params: gru_layers (int):
    :params: dropout (float):
    :params: logger_name (string): name of the logger
    :params: debug (bool): whether debugging info is printed or not
    """
    def __init__(self, seq_length, n_chars, embed_size=8, gru_units=128, gru_layers=1, dropout=0.2, 
                 logger_name="model", debug=False):
        super(CharRNN, self).__init__(seq_length, n_chars, embed_size, logger_name, debug)
        self.gru_units = gru_units
        self.gru_layers = gru_layers
        self.dropout = dropout
        
        # Feed into a GRU layer
        self.gru = nn.GRU(self.embed_size, self.gru_units, self.gru_layers, batch_first=True)
        self.drop = nn.Dropout(self.dropout)
        # Output a probability for each char, according to 
        # PyTorch's doc: This criterion combines nn.LogSoftmax() and nn.NLLLoss() in one single class
        # hence we must not use the softmax here.
        self.output = nn.Linear(self.gru_units, self.n_chars)
    
    def forward(self, X):
        self.logger.debug("X shape = %s\n" % repr(X.shape))
        X = self.embed(X)
        self.logger.debug("X embedding shape = %s\n" % repr(X.shape))
        Z, _  = self.gru(X)
        # Only keeping last time step prediction
        Z = self.drop(Z[:, -1, :])
        self.logger.debug("Z shape = %s\n" % repr(Z.shape))
        Y_hat = self.output(Z)
        self.logger.debug("Y_hat shape = %s\n" % repr(Y_hat.shape))
        return Y_hat


class CharHybrid(CharBaseModel):
    """
    CharHybrid
    ----------
    
    :params: seq_length (int): sequence length (# of timesteps)
    :params: n_chars (int): total number of characters used in the model (# of classes)
    :params: embed_size (int): size of the embedding to use for the characters
    :params:
    :params: logger_name (string): name of the logger
    :params: debug (bool): whether debugging info is printed or not
    """
    def __init__(self, seq_length, n_chars, embed_size=8, dropout=0.2, kernel_size=3, n_channels=32,
                 gru_layers=1, gru_units=32, logger_name="model", debug=False):
        super(CharHybrid, self).__init__(seq_length, n_chars, embed_size, logger_name, debug)

        self.kernel_size = kernel_size
        self.n_channels = n_channels
        self.gru_units = gru_units
        self.gru_layers = gru_layers
        self.dropout = dropout
        # Embed
        self.dilations = [3, 2, 1]
        self.conv1 = nn.Conv1d(self.embed_size, self.n_channels, self.kernel_size, dilation=self.dilations[0])
        self.conv2 = nn.Conv1d(self.n_channels, self.n_channels, self.kernel_size, dilation=self.dilations[1])
        self.conv3 = nn.Conv1d(self.n_channels, self.n_channels, self.kernel_size, dilation=self.dilations[2])
        self.pool = nn.AdaptiveMaxPool1d(self.embed_size)
        self.gru = nn.GRU(self.n_channels, self.gru_units, self.gru_layers,
                          batch_first=True)
        self.drop = nn.Dropout(self.dropout)
        self.output = nn.Linear(self.gru_units, self.n_chars)

    def forward(self, X):
        self.logger.debug("X shape = %s\n" % repr(X.shape))
        X = self.embed(X)
        self.logger.debug("X embedding shape = %s\n" % repr(X.shape))

        Z = X.transpose(1, 2)
        Z = torch.functional.F.relu(self.conv1(Z))
        Z = torch.functional.F.relu(self.conv2(Z))
        Z = torch.functional.F.relu(self.conv3(Z))
        Z = Z.transpose(1, 2)
        self.logger.debug("Z pool + conv + transpose shape = %s\n" % repr(Z.shape))

        Z, _ = self.gru(Z)
        Z = self.drop(Z[:, -1, :])
        self.logger.debug("Z gru shape = %s\n" % repr(Z.shape))

        Y_hat = self.output(Z)
        self.logger.debug("Y_hat shape = %s\n" % repr(Y_hat.shape))
        return Y_hat


class CharConv(CharBaseModel):
    """
    CharConv
    --------
    http://www.wildml.com/2015/12/implementing-a-cnn-for-text-classification-in-tensorflow/
    :params: seq_length (int): sequence length (# of timesteps)
    :params: n_chars (int): total number of characters used in the model (# of classes)
    :params: embed_size (int): size of the embedding to use for the characters
    :params:
    :params: logger_name (string): name of the logger
    :params: debug (bool): whether debugging info is printed or not
    """
    def __init__(self, seq_length, n_chars, embed_size=8, kernel_sizes=[3, 3, 3], n_channels=16, pool_size=10,
                 dropout=0.2, dilations=[3, 2, 1], logger_name="model", debug=False):
        super(CharConv, self).__init__(seq_length, n_chars, embed_size, logger_name, debug)

        self.kernel_sizes = kernel_sizes
        self.n_channels = n_channels
        self.dilations = dilations
        self.pool_size = pool_size
        self.dropout = dropout

        # Embed
        self.conv4 = nn.Conv1d(self.embed_size, self.n_channels, self.kernel_sizes[0], dilation=self.dilations[0])
        self.conv5 = nn.Conv1d(self.n_channels, self.n_channels, self.kernel_sizes[1], dilation=self.dilations[1])
        self.conv6 = nn.Conv1d(self.n_channels, self.embed_size, self.kernel_sizes[2], dilation=self.dilations[2])
        
        self.ada_pool1 = nn.AdaptiveMaxPool1d(self.pool_size)
        self.drop = nn.Dropout(self.dropout)
        temp = self.embed_size * self.pool_size
        self.lin1 = nn.Linear(temp, temp)
        self.output = nn.Linear(temp, self.n_chars)

    def forward(self, X):
        self.logger.debug("X shape = %s\n" % repr(X.shape))
        X = self.embed(X)
        self.logger.debug("X embedding shape = %s\n" % repr(X.shape))

        Z = X.transpose(1, 2)

        Z = torch.functional.F.relu(self.conv4(Z))
        Z = torch.functional.F.relu(self.conv5(Z))
        Z = self.ada_pool1(torch.functional.F.relu(self.conv6(Z)))
        self.logger.debug("Z conv 4-5-6 + pool shape = %s\n" % repr(Z.shape))
        Z = Z.flatten(start_dim=1, end_dim=2)
        self.logger.debug("Z flat shape = %s\n" % repr(Z.shape))

        # Add linear layers
        Z = torch.functional.F.relu(self.lin1(self.drop(Z)))
        Y_hat = self.output(Z)
        self.logger.debug("Y_hat shape = %s\n" % repr(Y_hat.shape))
        return Y_hat

        
class CharTCN(CharBaseModel):
    """
    CharTCN
    -------
    Fork of https://github.com/locuslab/TCN/blob/master/TCN/char_cnn/model.py
    """
    def __init__(self, n_chars, embed_size, num_channels, kernel_size=3,
                 dropout=0.2, logger_name="model", debug=False):
        super(CharTCN, self).__init__(None, n_chars, embed_size, logger_name, debug)

        self.num_channels = num_channels
        self.kernel_size = kernel_size
        self.dropout = dropout

        self.encoder = self.embed
        self.drop = nn.Dropout(self.dropout)
        self.tcn = TemporalConvNet(self.embed_size, self.num_channels, kernel_size=self.kernel_size,
                                   dropout=self.dropout)
        self.decoder = nn.Linear(self.embed_size, self.n_chars)
        self.decoder.weight = self.encoder.weight
        self.init_weights()

    def init_weights(self):
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.fill_(0)
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, X):
        # input has dimension (N, L_in), and emb has dimension (N, L_in, C_in)
        self.logger.debug("X shape = %s\n" % repr(X.shape))
        X = self.drop(self.encoder(X))
        self.logger.debug("X embedding shape = %s\n" % repr(X.shape))
        Z = self.tcn(X.transpose(1, 2))
        self.logger.debug("Z shape = %s\n" % repr(Z.shape))
        Y_hat = self.decoder(Z.transpose(1, 2)).contiguous()[:, -1, :]
        self.logger.debug("Y_hat shape = %s\n" % repr(Y_hat.shape))
        return Y_hat


if __name__ == "__main__":
    model = CharRNN(10, 10)
    model = CharBaseModel(10, 10)