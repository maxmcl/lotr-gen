import logging
import torch
import numpy as np
from preprocess import format_text

class LotRTrainer:
    def __init__(self, optimizer, n_epochs, model, print_every=100, patience=int(5e3), eval_every=100,
                 model_save_path="models/model.pt", debug=False):
        self.optimizer = optimizer
        
        assert n_epochs >= 1 and patience >= 1
        self.n_epochs = n_epochs
        self.model = model
        
        self.model_save_path = model_save_path
        self.accs = None
        self.losses = None
        self.best_loss = None
        self.acc_at_best_loss = None
        self.print_every = print_every
        self.eval_every = eval_every
        self.epoch = None
        self.epochs_no_improve = None
        self.patience = patience
        
        self.logger = logging.Logger("trainer")
        self.logger.setLevel(logging.DEBUG if debug else logging.INFO)
        self.logger.addHandler(logging.StreamHandler())
        
    def fit(self, gen):
        # gen is a generator that should be infinite or at least generate as many Xs and Ys
        # as there are epochs.
        self.losses = [np.inf]
        self.best_loss = np.inf
        self.accs = [0.0]
        self.acc_at_best_loss = 0.0
        self.epochs_no_improve = 0
        try:
            for epoch in range(self.n_epochs + 1):
                self.epoch = epoch
                # Get the data from the generator
                X, Y = next(gen)
                
                self.logger.debug("X shape = %s\n" % repr(X.shape))
                self.logger.debug("Y shape = %s\n" % repr(Y.shape))
                # Clear gradients
                self.model.zero_grad()
                # Compute forward pass
                Y_hat = self.model.forward(X)
                # Evaluate the loss
                loss = self.model.loss(Y_hat, Y)
                self.losses.append(loss.item())

                self.accs.append(LotRTrainer.acc(Y_hat, Y))
      
                # Take a gradient step
                loss.backward()
                self.optimizer.step()
                
                # Check if loss improved or if tolerance is exceeded
                if self._monitor_training():
                    self.logger.info("Patience reached, exiting\n")
                    break
                if (self.eval_every > 0) & (self.epoch % self.eval_every == 0):
                    # Keeping the dims of the tensor
                    temp = X[np.random.randint(0, X.shape[0]), :].reshape(1, -1)
                    self.logger.info("%s\n---" % repr(format_text(temp.squeeze().numpy())))
                    self.logger.info("%s\n" % repr(format_text(self.model.predict(temp)
                                                                   .squeeze().numpy())))
        except KeyboardInterrupt:
            print("Training stopped early.\n")
        return
    
    def _monitor_training(self):
        if (self.print_every > 0) & (self.epoch % self.print_every == 0):
            self.logger.info("\tepoch = %d\tloss = %.3f\tacc = %2f\t epochs no improve = %d\n\tbest loss = %.3f\tacc at best loss = %2f\n" % (self.epoch, self.losses[-1], self.accs[-1], self.epochs_no_improve, 
                                                                                                     self.best_loss, self.acc_at_best_loss))
        # Check for relative improvement
        if self.losses[-1] >= self.best_loss:
            self.epochs_no_improve += 1
        else:
            # Improved, reset counter
            self.epochs_no_improve = 0
            self.best_loss = self.losses[-1]
            self.acc_at_best_loss = self.accs[-1]
            torch.save(self.model.state_dict(), self.model_save_path)
        return True if self.epochs_no_improve > self.patience else False
        
    @staticmethod
    def acc(Y_hat, Y):
        return (Y_hat.argmax(dim=1) == Y).sum().item() / Y_hat.shape[0] * 100.0
