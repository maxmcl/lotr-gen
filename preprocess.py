import numpy as np
import glob
import re
import string
import torch


# All characters considered
all_chars = list(string.ascii_lowercase + " 1234567890.,!?")
n_chars = len(all_chars)
# Make a mapping of char -> index
char2ind_mapping = dict(zip(all_chars, range(n_chars)))
# Reverse mapping index -> char
ind2char_mapping = dict(enumerate(all_chars))


def _preprocess_file(file):
    """
    _preprocess_file
    ----------------
    Reads any of the LotR text files and preprocesses and cleans them.
    :params: file (handle to open file)
    :return: (string) file read, preprocessed and cleaned
    """
    temp = file.read()
    # Cropping useless beginning chunks
    temp = temp[re.search(r"\* BOOK.+\*\s", temp).end():]
    # Removing chapter headers and stuff between *
    temp = re.sub(r"Chapter \d.*?\n|\*.+\*", "", temp)
    # Replace any whitespace with a single space
    temp = re.sub(r"\s+", " ", temp)
    # Cleaning extra space and putting in lower case
    temp = temp.strip().lower()
    # Remove non-ascii characters
    temp = re.sub(r"[^a-z.,!?0-9 ]", "", temp)
    return temp


def load_data():
    """
    load_data
    ---------
    :return: whole LotR corpus preprocessed & cleaned into one string.
    """
    text = ""
    for filename in glob.glob("data/*.txt"):
        with open(filename, mode="r", encoding="utf-8") as file:
            text += _preprocess_file(file)
    return text


def _apply_mapping(arr, mapping):
    # Applies a mapping on an array and returns an array of same length
    return [mapping[a] for a in arr]


def char_to_ind(arr):
    """
    char_to_ind
    -----------
    Converts an array of characters to the corresponding indices defined in char2ind_mapping.
    :params: arr (array-like): array of characters
    :return: index-encoded array
    """
    # Must use int dtype to convert to LongType tensor for embedding
    return np.array(_apply_mapping(arr, char2ind_mapping), dtype=int)


def ind_to_char(arr):
    """
    ind_to_char
    -----------
    Converts an array of indices to the corresponding characters defined in ind2char_mapping.
    :params: arr (array-like): array of indices
    :return: character-encoded array
    """
    return np.array(_apply_mapping(arr, ind2char_mapping), dtype=str)


def format_text(arr):
    """
    format_text
    -----------
    Converts 1D array of ind to readable text
    :params: arr (array-like): array of indices
    :return: readable string
    """
    return "".join(ind_to_char(arr))


def gen_X_Y(text, seq_length=50, n_seqs=1):
    """
    gen_X_Y
    -------
    Infinite generator that yields n_seqs sequences of seq_length characters from a text corpus
    stored in an array. Y is the next character coming after the sequence.
    :params: text (Numpy array of int): index-encoded text corpus
    :params: seq_length (int): length of the sequences to generate (# of timesteps)
    :params: n_seqs (int): number of text sequences to generate (batch size)
    :return: (n_seqs, seq_length) X and (n_seqs, 1) Y PyTorch tensors.
    """
    while True:
        # Infinite generator
        assert text.shape[0] >= seq_length
        max_len = len(text) - seq_length
        # Generating n_seqs random initial positions between 0 and max_len
        all_pos = np.random.randint(0, max_len, size=n_seqs)
        # Creating n_seqs slices of length seq_length from initial positions
        ranges = np.array([np.arange(pos, pos + seq_length) for pos in all_pos])
        # Slicing (n_seqs, seq_length) and offsetting by 1 to predict the next character
        yield torch.from_numpy(text[ranges]), torch.from_numpy(text[ranges[:, -1] + 1]).reshape(-1)


def gen_X_Y_seqs(text, seq_length=50, n_seqs=1):
    """
    gen_X_Y_seqs
    ------------
    Infinite generator that yields n_seqs sequences of seq_length characters from a text corpus
    stored in an array. Y is a sequence of seq_length length containing the character following
    the character for a given timestep in X.
    :params: text (Numpy array of int): index-encoded text corpus
    :params: seq_length (int): length of the sequences to generate (# of timesteps)
    :params: n_seqs (int): number of text sequences to generate (batch size)
    :return: (n_seqs, seq_length) X and Y PyTorch tensors.
    """
    while True:
        # Infinite generator
        assert text.shape[0] >= seq_length
        max_len = len(text) - seq_length
        # Generating n_seqs random initial positions between 0 and max_len
        all_pos = np.random.randint(0, max_len, size=n_seqs)
        # Creating n_seqs slices of length seq_length from initial positions
        ranges = np.array([np.arange(pos, pos + seq_length) for pos in all_pos])
        # Slicing (n_seqs, seq_length) and offsetting by 1 to predict the next character
        yield torch.from_numpy(text[ranges]), torch.from_numpy(text[ranges + 1]).reshape(-1)
